# Exercise 30: Copy App to AWS Server

After preparing your AWS server by installing Node.js and npm, the next step is to deploy your Node.js application to this server. This exercise guides you through the process of securely copying your Node.js application package to your AWS server.

## Steps to Copy Your Node.js App to the Server

1. **Securely Copy Your Project File to the Server:**

    From your local machine, and within the project's root folder where your packaged Node.js application file (`bootcamp-node-project-1.0.0.tgz`) is located, use the `scp` command to copy it to your AWS server. Replace `{server-ip-address}` with the actual IP address of your AWS server.

    ```bash
    scp bootcamp-node-project-1.0.0.tgz root@{server-ip-address}:/root
    ```

    Note: The destination path and username (`root` in the example) might vary based on the server's configuration and the AMI used. For Amazon Linux, the default user might be `ec2-user`, and for Ubuntu, it might be `ubuntu`.

    Make sure to adjust the command with the correct username and path where you want to store the application on the server.

## Best Practices

- **Use Absolute Paths:** When copying files to a remote server, use absolute paths to specify the source and destination directories to avoid confusion and ensure the files end up in the correct location.
- **Secure Your Transfers:** Always use `scp` or `rsync` over SSH for file transfers to ensure that your data is encrypted and secure during transit.
- **Manage Permissions:** After transferring your application files, ensure that the file permissions on the server are correctly set to allow execution where necessary but restricted where not needed for security.

# Contributions

Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/cloud-and-iaas-basics-aws/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Utilized scp to securely transfer the Node.js application package to the AWS server, ensuring the app's readiness for deployment.
- Highlighted the importance of using absolute paths, securing data transfers, and managing file permissions for optimal security and functionality on the cloud server.