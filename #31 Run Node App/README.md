# Exercise 31: Run Node App on AWS Server

With your Node.js application successfully copied to your AWS server, the final step is to start the application. This exercise walks you through the process of unpacking the application, installing its dependencies, and running it in detached mode.

## Steps to Start the Node.js Application

### SSH into Your AWS Server

1. **Access your server via SSH:**

    Use the SSH command to securely log into your AWS server. Replace `{server-ip-address}` with your server's actual IP address.

    ```bash
    ssh -i ~/id_rsa root@{server-ip-address}
    ```

    Note: The path to the SSH key (`~/id_rsa`) and the username (`root`) may vary based on your server's setup and the SSH key you've configured.

### Prepare and Start the Application

2. **Unpack the Node.js application package:**

    Once logged in, use the `tar` command to extract your application package.

    ```bash
    tar -zxvf bootcamp-node-project-1.0.0.tgz
    ```

3. **Navigate to the unpacked application directory:**

    Change into the directory that was created by unpacking the package, typically named `package`.

    ```bash
    cd package
    ```

4. **Install the application dependencies:**

    Run `npm install` to install all dependencies specified in your `package.json` file.

    ```bash
    npm install
    ```

5. **Start the application:**

    Use the `node` command to start your application's main file (e.g., `server.js`). To run your application in detached mode, consider using tools like `pm2`, `forever`, or `nohup` for production environments. For the purposes of this exercise, a simple command to start the server will be:

    ```bash
    node server.js
    ```

## Best Practices

- **Use a Process Manager:** For production deployments, use a process manager like `pm2` to start your Node.js application. This ensures that your app restarts automatically in case of a crash and allows for easy management of logs and application processes.
- **Monitor Your Application:** Implement monitoring and alerting tools to keep track of your application's performance and health.
- **Secure Your Application:** Ensure that your application is secured against common vulnerabilities and that sensitive data, such as API keys and database credentials, are properly protected.

# Contributions

Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/cloud-and-iaas-basics-aws/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Accessed the AWS server via SSH and successfully unpacked and installed the Node.js application, setting the stage for execution.
- Launched the Node app, emphasizing the use of process managers for sustained operation and the importance of application security and monitoring in a production environment.