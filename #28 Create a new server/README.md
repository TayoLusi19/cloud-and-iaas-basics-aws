# Exercise 28: Create a New Server on AWS

This exercise guides you through the process of creating a new server instance on AWS (Amazon Web Services), the cloud infrastructure platform used by your company instead of on-premise servers.

## Steps to Create a New Server Instance on AWS

Creating a server instance on AWS involves launching a new EC2 (Elastic Compute Cloud) instance.

1. **Log in to your AWS Management Console** and navigate to the EC2 Dashboard.
2. **Click on the "Launch Instance" button** to start the process of creating a new instance.
3. **Choose an Amazon Machine Image (AMI)**, such as Amazon Linux, Ubuntu, or any other preferred Linux distribution.
4. **Select an Instance Type** that meets the requirements of your application in terms of CPU, memory, and networking capacity.
5. **Configure Instance Details** as needed, including network and subnet configurations.
6. **Add Storage** if the default storage does not meet your application’s requirements.
7. **Configure Security Group** to set up firewall rules. Ensure at least SSH (port 22) is allowed from your IP address for secure access.
8. **Review and Launch** the instance. You will be prompted to select a key pair for SSH access. You can use an existing key pair or create a new one.
9. **Launch the Instance**. Your new server will be initialized and will be accessible shortly.

# Contributions

Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/cloud-and-iaas-basics-aws/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Successfully navigated AWS Management Console to launch an EC2 instance, selecting an appropriate AMI and instance type for server setup.
- Configured instance details, added storage, set up security groups for SSH access, and launched the new server, ensuring readiness for application deployment.