# Exercise 27: Package NodeJS App into a Tar File

Packaging a Node.js application into a tar file is a streamlined way to create a distributable artifact of your application. This can be particularly useful for deployment or sharing your app with others. The `npm pack` command bundles up your application, including all the files listed in your `package.json`, into a tarball.

## Steps to Package Your Node App

### Navigate to Your App Directory

1. **Change into your application's directory:**

    Before packaging your app, make sure you are in the app's root directory where the `package.json` file is located.

    ```bash
    cd app
    ```

    Replace `app` with the actual directory name of your Node.js application if it's different.

### Package the Application

2. **Run `npm pack` to create a tar file of your app:**

    Execute the `npm pack` command to generate a tarball (`*.tgz` file) that contains the entire contents of your application as specified in `package.json`.

    ```bash
    npm pack
    ```

    After running this command, you'll find a `.tgz` file in your app directory. This file is named according to the "name" and "version" fields in your `package.json` (e.g., `your-app-name-1.0.0.tgz`).

## Best Practices

- **Verify `package.json`:** Before packaging your app, ensure your `package.json` accurately reflects the application's dependencies, scripts, and metadata. This includes specifying which files to include or exclude from the package through the `files` array or `.npmignore` file.
- **Test the Packaged App:** After packaging, it's a good idea to test the tarball in a clean environment to ensure it contains all necessary files and dependencies. You can do this by creating a new directory, unpacking the tarball with `npm install <tarball file>`, and running your application.
- **Versioning:** Keep your application versioning up to date in `package.json`. This practice is crucial for maintaining and distributing new versions of your app.

By following these steps, you can efficiently package your Node.js application into a tar file, making it ready for distribution or deployment.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/cloud-and-iaas-basics-aws/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Packaged Node.js application into a tar file using npm pack, creating a distributable .tgz artifact based on package.json.
- Emphasized best practices like verifying package.json, testing the tarball, and maintaining version accuracy for effective app distribution.