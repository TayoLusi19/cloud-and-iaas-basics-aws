# Exercise 32: Access Application from Browser - Configure Firewall on AWS

Once your Node.js application is running on the AWS server, the next step is to make it accessible from the web. This typically involves configuring the server's firewall to allow traffic on the port your application is using (commonly port 80 for HTTP or port 443 for HTTPS, or any custom port your app listens on).

## Steps to Open the Correct Port on AWS

### Configure Security Groups in AWS

1. **Navigate to the AWS Management Console:**

    Log in to your AWS account and go to the EC2 Dashboard.

2. **Find Your Instance's Security Group:**

    - In the EC2 Dashboard, look for the "Instances" section and select your running instance.
    - In the "Description" tab below, find the "Security groups" field and click on the security group's name linked to your instance.

3. **Edit Inbound Rules of the Security Group:**

    - Inside the Security Group dashboard, navigate to the "Inbound rules" tab.
    - Click on the "Edit inbound rules" button to modify the rules.

4. **Add a New Rule to Open the Application's Port:**

    - Click on "Add rule".
    - For "Type", select "Custom TCP" if your application runs on a custom TCP port or choose "HTTP" (port 80) / "HTTPS" (port 443) for standard web applications.
    - For "Port range", enter the port number your Node.js application listens on.
    - For "Source", select "Anywhere" to allow access from any IP address, or specify a more restrictive set of IPs for enhanced security.
    - Click "Save rules" to apply the changes.

### Access the Application UI from the Browser

5. **Access Your Application:**

    - Open a web browser and navigate to `http://{server-ip-address}:{port}` if using HTTP or `https://{server-ip-address}:{port}` for HTTPS, replacing `{server-ip-address}` with your AWS server's public IP address and `{port}` with the port number your application uses.

    - You should now be able to access your application's UI through the browser.

## Best Practices

- **Minimal Exposure:** Only open the ports necessary for your application's operation to minimize the server's exposure to potential attacks.
- **Regularly Review Security Groups:** Periodically review and update your security group rules to ensure they meet your application's current needs and maintain security.
- **Use HTTPS:** For applications accessible on the internet, prefer using HTTPS (port 443) with a valid SSL certificate to encrypt traffic between the client and the server.

# Contributions

Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/cloud-and-iaas-basics-aws/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Configured AWS security groups to allow web access, successfully opening the necessary port for the Node.js application.
- Accessed the application through the browser, demonstrating the importance of minimal port exposure and the benefits of HTTPS for secure web access.