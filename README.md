# Cloud And IaaS Basics - AWS



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/TayoLusi19/cloud-and-iaas-basics-aws.git
git branch -M main
git push -uf origin main
```

# Resume Bullet Point


## Exercises 26
- Cloned an existing repository and removed its Git history to start a fresh project version control timeline.
- Created and pushed a new repository to GitLab, establishing a personalized project space for development.

## Exercises 27
- Packaged Node.js application into a tar file using npm pack, creating a distributable .tgz artifact based on package.json.
- Emphasized best practices like verifying package.json, testing the tarball, and maintaining version accuracy for effective app distribution.

## Exercises 28
- Successfully navigated AWS Management Console to launch an EC2 instance, selecting an appropriate AMI and instance type for server setup.
- Configured instance details, added storage, set up security groups for SSH access, and launched the new server, ensuring readiness for application deployment.

## Exercises 29
- Accessed the AWS server via SSH and successfully installed Node.js and npm, setting the stage for Node application deployment.
- Emphasized security and regular updates as best practices for server management and application hosting.

## Exercises 30
- Utilized scp to securely transfer the Node.js application package to the AWS server, ensuring the app's readiness for deployment.
- Highlighted the importance of using absolute paths, securing data transfers, and managing file permissions for optimal security and functionality on the cloud server.

## Exercises 31
- Accessed the AWS server via SSH and successfully unpacked and installed the Node.js application, setting the stage for execution.
- Launched the Node app, emphasizing the use of process managers for sustained operation and the importance of application security and monitoring in a production environment.

## Exercises 32
- Configured AWS security groups to allow web access, successfully opening the necessary port for the Node.js application.
- Accessed the application through the browser, demonstrating the importance of minimal port exposure and the benefits of HTTPS for secure web access.