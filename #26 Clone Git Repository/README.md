# Exercise 26: Clone Git Repository and Setup Your Own

This exercise guides you through cloning an existing Git repository and creating your own repository from it. This is a common practice when you want to start a new project based on an existing codebase.

## Steps to Clone and Set Up Your Own Repository

### Clone the Repository

1. **Clone the repository to your local machine:**

    Use the following command to clone the repository. Replace the URL with the provided Git repository URL if different.

    ```bash
    git clone git@gitlab.com:twn-devops-bootcamp/latest/05-cloud/cloud-basics-exercises.git
    ```

2. **Navigate to the project directory:**

    After cloning, change into the project directory. The directory name might vary based on the repository. The example given is `node-project`, but ensure you use the correct directory name as provided in the clone command's output.

    ```bash
    cd node-project
    ```

### Create Your Own Local Repository

3. **Remove the original `.git` directory:**

    This step is crucial as it removes the link to the original repository, allowing you to start a new Git history for your project.

    ```bash
    rm -rf .git
    ```

4. **Initialize a new Git repository:**

    Create a new `.git` directory and start your own version control history.

    ```bash
    git init
    ```

5. **Add the project files to the repository and commit:**

    Add all existing files to the staging area and make your first commit to your new repository.

    ```bash
    git add .
    git commit -m "Initial commit"
    ```

### Push Your Repository to GitLab

6. **Create a new repository on GitLab:**

    Log in to your GitLab account and create a new repository. Navigate to your dashboard and select "New project" or "Create a project".

7. **Link your local repository to your new GitLab repository:**

    Replace `{gitlab-user}` and `{gitlab-repo}` with your GitLab username and the new repository name, respectively.

    ```bash
    git remote add origin git@gitlab.com:{gitlab-user}/{gitlab-repo}.git
    ```

8. **Push your local repository to GitLab:**

    This command pushes your commits to the master branch of your remote repository on GitLab, setting it as the upstream for future pushes.

    ```bash
    git push -u origin master
    ```

## Conclusion

By completing these steps, you have successfully cloned an existing repository, removed its Git history, and started your own repository based on it. This process is useful for creating new projects from templates or existing codebases.

Remember to replace placeholders like `node-project`, `{gitlab-user}`, and `{gitlab-repo}` with actual values relevant to your project and GitLab account.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/cloud-and-iaas-basics-aws/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Cloned an existing repository and removed its Git history to start a fresh project version control timeline.
- Created and pushed a new repository to GitLab, establishing a personalized project space for development.