# Exercise 29: Prepare Server to Run Node App

With your AWS server instance set up, the next step is to prepare it for running a Node.js application by installing Node.js and npm.

## Steps to Install Node.js and npm

1. **SSH into Your Newly Created Server:**

    Replace `{server-ip-address}` with the public IP address of your AWS server instance.

    ```bash
    ssh -i /path/to/your/key.pem root@{server-ip-address}
    ```

    Note: The exact SSH command might vary based on the AMI used and the username might not be `root` (e.g., `ec2-user` for Amazon Linux, `ubuntu` for Ubuntu).

2. **Install Node.js and npm:**

    First, update your package list. Then, install Node.js and npm using the package manager of your Linux distribution. The example below uses `apt` for Debian-based distributions like Ubuntu.

    ```bash
    sudo apt update
    sudo apt install -y nodejs npm
    ```

    This command installs the latest version of Node.js and npm available in the package repository.

## Best Practices

- **Security:** Utilize Security Groups effectively to minimize exposure to unnecessary risk.
- **SSH Key Management:** Safeguard your SSH keys, as they provide administrator access to your instance.
- **Regular Updates:** Keep your server's software up to date with the latest security patches and updates.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/cloud-and-iaas-basics-aws/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Accessed the AWS server via SSH and successfully installed Node.js and npm, setting the stage for Node application deployment.
- Emphasized security and regular updates as best practices for server management and application hosting.